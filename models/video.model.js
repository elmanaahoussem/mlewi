const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VideoSchema = new Schema({ 
    name: {type: String, required: true},
    description: {type: String},
    tags : {type: String},
    category : {type: String},
    thumbnail :{type: String},
    link :{type: String},
    source : {type: String},
    date : {type: String} ,
    k2s: {type: String},
    gounlimited:{type: String},
    idfront : {type: String},
    urlfront : {type:String},
});


module.exports = mongoose.model('Video', VideoSchema);