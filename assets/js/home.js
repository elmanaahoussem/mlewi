var socket = socket = io.connect();  
var ScreenElement = $('#question')
var fastDashboard = $('#fast-result')
 
var players = []

socket.on('pausemedia',function (data) {
     $("#lecteurAudio")[0].pause()
     $("#lecteurVideo")[0].pause() 
    
} )

socket.on('playVideo',function(data){ 
    $('#question').addClass("d-none")
    $("#lecteurAudio")[0].pause()
    $('#lecteurcontainer').removeClass("d-none") 
    $('#lecteurVideo').removeClass("d-none") 
    $('#lecteurVideo source').attr('src','videos/' + data.video)
    $("#lecteurVideo")[0].load();
    $("#lecteurVideo")[0].play();  
}) 

socket.on('playAudio',function(data){ 
    $('#lecteurAudio source').attr('src','audios/' + data.audio)
    $("#lecteurAudio")[0].load();
    $("#lecteurAudio")[0].play();  
}) 

socket.on('questionSent',function(data){  
    $('#lecteurcontainer').addClass("d-none")
    $('#question').removeClass("d-none")  
    if (data.question.type == "multiple"){
        renderQuestion(data.question,ScreenElement)
    }else {
        renderInputQuestion(data.question,ScreenElement)
    } 
}) 

socket.on('questionDisplay',function(data){  
    $('#lecteurcontainer').addClass("d-none")
    $('#question').removeClass("d-none")  
    if (data.question.type == "multiple"){
        renderQuestion(data.question,ScreenElement)
    }else {
        renderInputQuestion(data.question,ScreenElement)
    } 
}) 

socket.on('rapidQuestionSent',function(data){  
    $('#lecteurVideo').addClass("d-none")
    $('#question').removeClass("d-none")   
    renderQuestion(data.question,ScreenElement) 
	console.log(fastDashboard[0])
	fastDashboard[0].innerHTML =''
}) 

  
socket.on('updateTeams',function(data){
    renderTeams(data.client)
}) 


socket.on('rapidAnsweredHomeDash',function(data){
    console.log("eeeeeeeeeeeez",data)
	console.log(fastDashboard[0])
	fastDashboard[0].innerHTML += '<p>'+data.data.teamname +' : ' + ( data.resp.answerTrue ? 'Correct' : 'Faux' ) + '   ->  ' + data.data.timeStamp +'<p>'
}) 






function renderTeams (teamsArray) {
    clientsHTML = ""
    players = [] 
    teamsArray.map( (e) => { 
        clientsHTML += '<tr><td><p><strong>'+e.customId+'</strong></p></td><td><p>'+e.score +' <img src="/imgs/dma9.png" class="dma9"></p></td></tr>'
        players.push (e)
    })  
    $('#teamsSection').html(clientsHTML)
}


function renderInputQuestion (question,elementJquery) {
     questionHTML = '<div data-questionid="' + question.id + '">'
     questionHTML += '<h2 class="thequestion">' + question.question + '</h2> </div>' 
     elementJquery.html(questionHTML) 
}

function renderQuestion (question,elementJquery) { 
     questionHTML = '<div data-questionid="' + question.id + '">'
     questionHTML += '<h2 class="thequestion w-75">' + question.question + '</h2><ol class="propositions">'
     propositions = ""
        for (let index = 0; index < question.propositions.length; index++) {
             propositions += '<li onclick="submitQuestionAnswer('+"'"+question.id+"'"+','+index+','+"'"+question.phase+"'"+')" id='+index+'>' + question.propositions[index] + '</li>'  
        }  
    questionHTML += propositions + "</ol>"   
    if (question.image)
    {
          questionHTML += ' <img src="/imgs/'+question.image+'" class="questionimg" />'   
    }

    questionHTML += '</div>'
    elementJquery.html(questionHTML)    
}

 