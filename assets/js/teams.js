var socket = socket = io.connect();  
var ScreenElement = $('#screen')

var timer = null 
var timerMechanism = null 
var teamname = ''

function startTimer () {
    var timeleft = 60;
    timerMechanism = setInterval(function(){
    timeleft--;
    document.getElementById("timer").textContent = timeleft;
    if(timeleft <= 0)
        clearInterval(timerMechanism);
    },1000);
}

function stopTimer () {
      clearInterval(timerMechanism);
}
 
$('#saveTeamName').on('click',function(e){ 
     teamname = $('#teamname').val() 
    $('#formMenu').toggleClass('d-none')
    $('#teamID').html(teamname)
    $('#dashboard').toggleClass('d-none') 
    socket.emit('storeClientInfo', { customId: teamname });
});

socket.on('questionSent',function(data){ 
    if (data.socketID == socket.id){ 
         if (data.question.type == "multiple"){
                renderQuestion(data.question,ScreenElement)
         }else {
               renderInputQuestion(data.question,ScreenElement)
         }
       
    } 
}) 

socket.on('rapidQuestionSent',function(data){   
    renderQuestion(data.question,ScreenElement) 
}) 

socket.on('questionAnswered',function(data){ 
   if (data.socketID == socket.id )
     {
          if (data.answerTrue)
          {
               $('#screen').html('<h1 class="correct"> CORRECT </h1> ')
          }else {
               $('#screen').html(' <h1 class="wrong"> FAUX </h1> ')
          } 
     }
}) 
 
socket.on('rapidAnswered',function(data){   
     if (data.socketID == socket.id )
     {
          if (data.answerTrue)
          {
               $('#screen').html('<h1 class="correct"> CORRECT </h1> ')
          }else {
               $('#screen').html('<h1 class="wrong"> FAUX </h1> ')
          } 
     }else {
          $('#screen').html('<h1 class="questionrepondu">  la question a été répondue </h1> ')
          clearTimeout(timer)
          stopTimer()
     } 
}) 
 

function renderInputQuestion (question,elementJquery) {
      questionHTML = '<div data-questionid="' + question.id + '">'
     questionHTML += '<h2 class="thequestion">' + question.question + '</h2> ' 
     questionHTML += '<input type="number" id="questionAnswer" /><br> <button class="abtn" onclick="submitAnswerInput('+"'"+question.id+"'"+' , '+"'"+question.phase+"'"+')" >Valider</button>'    
     elementJquery.html(questionHTML) 
     startTimer () 
     timer = setTimeout( function() { submitAnswerInput( question.id,question.phase,'timeout'  )  }, 61000);
      
}

function renderQuestion (question,elementJquery) { 
     questionHTML = '<div data-questionid="' + question.id + '">'
     questionHTML += '<h2 class="thequestion  w-75">' + question.question + '</h2><ol class="propositions">'
     propositions = ""
        for (let index = 0; index < question.propositions.length; index++) {
             propositions += '<a href="#"><li onclick="submitQuestionAnswer('+"'"+question.id+"'"+','+index+','+"'"+question.phase+"'"+')" id='+index+'>' + question.propositions[index] + '</li></a>'  
        }  
        questionHTML += propositions + "</ol>"   
    if (question.image)
    {
          questionHTML +=  ' <img src="/imgs/'+question.image+'" class="questionimg" />'   
    }

    questionHTML += '</div>'  
    elementJquery.html(questionHTML)  
    startTimer ()  
    timer = setTimeout( function() { submitQuestionAnswer( question.id , 99 , question.phase  )  }, 61000);  
    $(document).on("keypress", function (e) {
          if (e.which == 49) 
          {
               submitQuestionAnswer( question.id , 0 , question.phase  )
          }
            if (e.which == 50 ) 
          {
               submitQuestionAnswer( question.id , 1 , question.phase  )
          }
            if (e.which ==  51 ) 
          {
               submitQuestionAnswer( question.id , 2 , question.phase  )
          }
            if (e.which ==  52 ) 
          {
               submitQuestionAnswer( question.id , 3 , question.phase  )
          }
     });
     
}

function submitQuestionAnswer (questionId,index,phase) {
     console.log('Question iD : ' + questionId +  ' Response Number ' + index )
     socket.emit('answerOnAQuestion', {
         questionId : questionId,
         answer : index,
         socketID : socket.id,
         phase : phase,
		 timeStamp : Date.now(),
		 teamname
         })
     $('#screen').html('')
      $(document).off("keypress")
      clearTimeout(timer)
      stopTimer()

}

function submitAnswerInput (questionId , phase , timeout = null ) {
     if (!timeout)
     {
          socket.emit('answerOnAQuestion', {
               questionId : questionId,
               answer : $('#questionAnswer').val() ,
               socketID : socket.id,
               phase : phase,
			   timeStamp : Date.now(),
			   teamname
         })
     }else {
               socket.emit('answerOnAQuestion', {
               questionId : questionId,
               answer : '99999999' ,
               socketID : socket.id,
               phase : phase,
			   timeStamp : Date.now(),
			   teamname
         })
     } 
     $('#screen').html('')
     clearTimeout(timer)
     stopTimer()
}