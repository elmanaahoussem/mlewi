var socket = (socket = io.connect());
var questions = [];
var players = [];

$.getJSON("/json/questionsfake.json", function (data) {
  questions = data;
}).done(function () {
  renderQuestions(questions);
});

socket.on("updateTeams", function (data) {
  renderTeams(data.client);
});

socket.emit("getTeams", { data: "null" });

$("#stopAll").on("click", function (e) {
  socket.emit("stopMedia", { data: "null" });
});

$("#sendpoints").on("click", function (e) {
  adjustScore();
  $("#teampoints").val("");
});

$(".videobtn").on("click", function (e) {
  console.log("ee");
  socket.emit("playVideo", $(this).data("video"));
});

$(".audiobtn").on("click", function (e) {
  socket.emit("playAudio", $(this).data("audio"));
});

function toScreenOnly(parent, element, phase) {
  let selector = "#" + parent + " #" + element;
  let value = $(selector).val();
  socket.emit("questionShow", {
    question: getQuestionByIdAndPhase(value, phase),
  });
}

function renderQuestions(questions) {
  questionsphase1HTML = "";
  questionsphase2HTML = "";
  questionsphase3HTML = "";
  questions.map((e) => {
    if (e.phase == 1)
      questionsphase1HTML +=
        '<option value="' + e.id + '">Question Numéro ' + e.id + "</option>";
    else if (e.phase == 2)
      questionsphase2HTML +=
        '<option value="' + e.id + '">Question Numéro ' + e.id + "</option>";
  });
  $("#phase01 #questionsSelect").html(questionsphase1HTML);
  $("#phase02 #questionsSelect").html(questionsphase2HTML);
}

function dispatchQuestion() {
  value = $("#phase01 #questionsSelect").val();
  team = $("#phase01 #teamSelect").val();
  socket.emit("questionSent", {
    socketID: team,
    question: getQuestionByIdAndPhase(value, 1),
  });
  questions = questions.filter((e) => {
    return !(e.id == value && e.phase == 1);
  });
  renderQuestions(questions);
}
function adjustScore() {
  value = parseInt($("#teampoints").val());
  team = $("#teamSelect2").val();
  socket.emit("teamScore", { socketID: team, score: value });
}

function dispatchAll() {
  value = $("#phase02 #questionsSelect").val();
  socket.emit("rapidQuestionSent", {
    question: getQuestionByIdAndPhase(value, 2),
  });
  questions = questions.filter((e) => {
    return !(e.id == value && e.phase == 2);
  });
  renderQuestions(questions);
}

function renderTeams(teamsArray) {
  teamsHTML = "";
  players = [];
  teamsArray.map((e) => {
    teamsHTML +=
      '<option value="' + e.clientId + '"> ' + e.customId + "</option>";
    players.push(e);
  });

  $("#phase01 #teamSelect").html(teamsHTML);
  $("#teamSelect2").html(teamsHTML);
}

function getQuestionByIdAndPhase(id, phase) {
  return questions.filter((e) => {
    return e.id == id && e.phase == phase;
  })[0];
}
