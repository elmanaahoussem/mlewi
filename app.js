const express = require('express')
const app = express();
const server = require('http').createServer(app)
const io = require('socket.io').listen(server)
const fs = require('fs');

let wrongVideos = []
let correctVideos = []

fs.readdir('assets/videos/right/', (err, files) => {
  files.forEach(file => { 
    correctVideos.push(file) 
  });
 
});

fs.readdir('assets/videos/wrong/', (err, files) => {
  files.forEach(file => {
    wrongVideos.push(file)
  });
});


// Loads Questions
let rawdata = fs.readFileSync('assets/json/questionsfake.json');
let questions = JSON.parse(rawdata);

app.use(express.static('assets'))

clients = []; 
connections = [];
  
server.listen(80);
console.log('Server Running on 80 ... ')

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
});

app.get('/admin', (req, res) => {
  res.sendFile(__dirname + '/admin.html')
});

app.get('/team', (req, res) => {
  res.sendFile(__dirname + '/team.html')
});

function getClientBySocket(socket) {
  for (let index = 0; index < clients.length; index++) {
      if (clients[index].clientId == socket.id )
        return clients[index];
  }
}


function updateClientScore (socketID,score) {
  for (let index = 0; index < clients.length; index++) {
      if (clients[index].clientId == socketID )
         clients[index].score += score ;
  }
}

function getClientBySocketID(socketId) {
  for (let index = 0; index < clients.length; index++) {
      if (clients[index].clientId == socketId )
        return clients[index];
  }
}

function getSocketIdByTeamID(name) {
  for (let index = 0; index < clients.length; index++) {
    if (clients[index].customId == name ) 
      return clients[index].clientId;
  } 
}

function getQuestionByIdAndPhase (id,phase) { 
    return questions.filter( (e) => { return(   ( ( e.id == id) && ( e.phase == phase ) ) )  })[0]
}


function getTrueVideo (correctVideos) { 
    let v = correctVideos[Math.floor(Math.random() * correctVideos.length)];


    return 'right/'+ v
}

function getFalseVideo (wrongVideos) {
     let v = wrongVideos[Math.floor(Math.random() * wrongVideos.length)]; 
    return 'wrong/'+ v
}
 
io.sockets.on('connection',function (socket){
  connections.push(socket); 
  console.log('Connected : %s sockets connected' , connections.length) 

  // HandShake and Infos
  socket.on('storeClientInfo', function (data) { 
        var clientInfo = new Object();
        clientInfo.customId = data.customId ;
        clientInfo.clientId = socket.id ;
        clientInfo.score = 0 ;
        clients.push(clientInfo);  
        io.sockets.emit('updateTeams',{client:clients})
  });
 
  socket.on('answerOnAQuestion',(data) => { 

    client = getClientBySocketID(data.socketID) 
    question = getQuestionByIdAndPhase(data.questionId,data.phase)
    resp = Object()
    resp.socketID = data.socketID

    console.log('data Sent' , data , "Question data " , question)

    if (question.phase == 1) 
    {
      if (question.type == "multiple")
      {
        resp.answerTrue = (data.answer == question.answer )  
      }else {
        resp.answerTrue = (data.answer >= question.min && data.answer <= question.max )
      }   
        updateClientScore(data.socketID, (resp.answerTrue) ? question.points : 0  )

        io.sockets.emit('updateTeams',{client:clients}) 
        io.sockets.emit('questionAnswered',resp) 
    }else { 
        resp.answerTrue = (data.answer == question.answer) 
		    io.sockets.emit('rapidAnswered',resp) 
        
        //updateClientScore(data.socketID, (resp.answerTrue) ? question.points : -2  )
        io.sockets.emit('updateTeams',{client:clients})
        console.log('HEre')
        io.sockets.emit('rapidAnsweredHomeDash', {resp, data   , answerTrue : resp.answerTrue } )
    }

     io.sockets.emit('playVideo',{video: (resp.answerTrue) ? getTrueVideo(correctVideos) : getFalseVideo(wrongVideos) })
  })

  socket.on('getTeams',function () {
 
     io.sockets.emit('updateTeams',{client:clients})
  })

  socket.on('teamScore',function (data) { 
     updateClientScore(data.socketID, data.score  )
     io.sockets.emit('updateTeams',{client:clients})
  })
 
  socket.on('stopMedia',function (data) {
    io.sockets.emit('pausemedia',data) 
  })
 
  socket.on('questionShow',function(data){  
     io.sockets.emit('questionDisplay',data) 
  })  
  // Dispatching Questions
  socket.on('questionSent',function(data){  
     io.sockets.emit('questionSent',data) 
	 // document.querySelector('.')
  })   
  socket.on('rapidQuestionSent',function(data){  
     io.sockets.emit('rapidQuestionSent',data) 
  }) 
  
  // Playing Videos And Audios in Home Page
  socket.on('playVideo',function(data) {
    io.sockets.emit('playVideo',{video:data})
  })
  socket.on('playAudio',function(data) {
    io.sockets.emit('playAudio',{audio:data})
  })
 
  socket.on('disconnect',(data) => {
    connections.splice(connections.indexOf(socket),1)
    clients = clients.filter (function (client) {
        return (!(client.clientId == socket.id ))
    })
    //clients.splice(clients.indexOf(getClientBySocket(socket)),1)
    io.sockets.emit('updateTeams',{client:clients})
    console.log('Disconnected : %s sockets connected' , connections.length)
  })

})